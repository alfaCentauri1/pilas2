package com.alfaCentauri;

import java.util.Arrays;
import java.util.Stack;

public class Ejecutable {
    public static void main(String[] args){
        Stack<String> pilaLineas = new Stack<>();
        pilaLineas.push("primera");
        pilaLineas.push("segunda");
        System.out.println("Pruebas de la clase Satck");
        System.out.println("Con el forEach, pilaLineas tiene: " );
        pilaLineas.forEach(element -> {
            System.out.println("Elemento: " + element);
        });
        String valor = pilaLineas.pop();
        System.out.println("Con el pop, pilaLineas tiene: " );
        System.out.println("Elemento: " + valor);
    }
}
